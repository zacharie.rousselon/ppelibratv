<?php
class utilisateur
{
    private $idUser;
    private $nom;
    private $prenom;
    private $login;
    private $mdp;
    private $statut;
    
    
    public function __construct($unLogin,$unMdp){
        $this->login=$unLogin;
        $this->mdp=$unMdp;
    }
    
    public function getIdUser(){
        return $this->idUser;
    }
    
    public function setIdUser($unIdUser){
        $this->idUser =  $unIdUser;
    }
    
    public function getLogin(){
        return $this->login;
    }
    
    public function setLogin($unLogin){
        $this->login =  $unLogin;
    }
    
    public function getMdp(){
        return $this->mdp;
    }
    
    public function setMdp($unMdp){
        $this->mdp =  $unMdp;
    }
    
    
    
}