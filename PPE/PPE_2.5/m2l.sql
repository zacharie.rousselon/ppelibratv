-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: Sep 11, 2020 at 01:34 PM
-- Server version: 8.0.18
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `m2l`
--

-- --------------------------------------------------------

--
-- Table structure for table `bulletin`
--

DROP TABLE IF EXISTS `bulletin`;
CREATE TABLE IF NOT EXISTS `bulletin` (
  `idbulletin` varchar(5) NOT NULL,
  `mois` int(11) NOT NULL,
  `annee` int(11) NOT NULL,
  `bulletinPDF` varchar(50) NOT NULL,
  `idContrat` varchar(5) NOT NULL,
  PRIMARY KEY (`idbulletin`),
  KEY `bulletin_contrat_FK` (`idContrat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bulletin`
--

INSERT INTO `bulletin` (`idbulletin`, `mois`, `annee`, `bulletinPDF`, `idContrat`) VALUES
('B01', 12, 2020, '', 'CT1'),
('B02', 3, 2021, '', 'CT2'),
('B03', 1, 2021, '', 'CT3'),
('B04', 9, 2020, '', 'CT4'),
('B05', 3, 2020, '', 'CT5'),
('B06', 4, 2020, '', 'CT5'),
('B07', 5, 2020, '', 'CT5'),
('B08', 6, 2020, '', 'CT5'),
('B09', 6, 2021, '', 'CT6'),
('B10', 7, 2021, '', 'CT6'),
('B11', 7, 2020, '', 'CT4'),
('B12', 8, 2020, '', 'CT4'),
('B13', 11, 2020, '', 'CT2'),
('B14', 12, 2020, '', 'CT2');

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

DROP TABLE IF EXISTS `club`;
CREATE TABLE IF NOT EXISTS `club` (
  `idClub` varchar(5) NOT NULL,
  `nomClub` varchar(50) NOT NULL,
  `adresseClub` varchar(50) NOT NULL,
  `idLigue` varchar(5) NOT NULL,
  `IdCommune` int(11) NOT NULL,
  PRIMARY KEY (`idClub`),
  KEY `club_ligue_FK` (`idLigue`),
  KEY `club_Commune0_FK` (`IdCommune`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`idClub`, `nomClub`, `adresseClub`, `idLigue`, `IdCommune`) VALUES
('C1', 'Judo Club Antibes', '33bd gutstave chancel', 'L2', 1),
('C2', 'Karaté NA', '11 rue El Alamein', 'L1', 3),
('C3', 'Wushu NA', '11 rue El Alamein', 'L1', 3),
('C4', 'Rugby sud est', '600 Av. de la Valérane', 'L2', 1),
('C5', 'Club de Foot Ile de France', '5, Place de Valois', 'L3', 2),
('C6', 'Club de Basket Ile De France', '10 Place d\'Alnwick', 'L3', 2);

-- --------------------------------------------------------

--
-- Table structure for table `commune`
--

DROP TABLE IF EXISTS `commune`;
CREATE TABLE IF NOT EXISTS `commune` (
  `IdCommune` int(11) NOT NULL,
  `CodePostal` int(11) NOT NULL,
  `NomCommune` varchar(50) NOT NULL,
  `CodeDepartement` int(11) NOT NULL,
  PRIMARY KEY (`IdCommune`),
  KEY `Commune_Departement_FK` (`CodeDepartement`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commune`
--

INSERT INTO `commune` (`IdCommune`, `CodePostal`, `NomCommune`, `CodeDepartement`) VALUES
(1, 6600, 'Antibes', 6),
(2, 75000, 'Paris', 2),
(3, 33800, 'Bordeaux', 3);

-- --------------------------------------------------------

--
-- Table structure for table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
CREATE TABLE IF NOT EXISTS `contrat` (
  `idContrat` varchar(5) NOT NULL,
  `dateDebut` date NOT NULL,
  `dateFin` date NOT NULL,
  `typeContrat` varchar(20) NOT NULL,
  `nbHeures` int(11) NOT NULL,
  `idUser` varchar(5) NOT NULL,
  PRIMARY KEY (`idContrat`),
  KEY `contrat_utilisateur_FK` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contrat`
--

INSERT INTO `contrat` (`idContrat`, `dateDebut`, `dateFin`, `typeContrat`, `nbHeures`, `idUser`) VALUES
('CT1', '2020-09-09', '2020-10-31', 'CDD', 32, 'U02'),
('CT2', '2020-09-18', '2021-02-20', 'CDD', 28, 'U03'),
('CT3', '2020-11-13', '2020-12-12', 'Mission Interim', 24, 'U01'),
('CT4', '2020-05-10', '2020-08-26', 'CDD', 34, 'U01'),
('CT5', '2020-02-17', '2020-12-11', 'Mission Interim', 28, 'U02'),
('CT6', '2021-05-11', '2021-07-23', 'CDD', 29, 'U03');

-- --------------------------------------------------------

--
-- Table structure for table `demander`
--

DROP TABLE IF EXISTS `demander`;
CREATE TABLE IF NOT EXISTS `demander` (
  `idForma` varchar(5) NOT NULL,
  `idUser` varchar(5) NOT NULL,
  `EtatDemande` varchar(50) NOT NULL,
  PRIMARY KEY (`idForma`,`idUser`),
  KEY `Demander_utilisateur0_FK` (`idUser`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `demander`
--

INSERT INTO `demander` (`idForma`, `idUser`, `EtatDemande`) VALUES
('F01', 'U01', 'refusé'),
('F01', 'U02', 'Accepté'),
('F02', 'U01', 'refusée'),
('F02', 'U03', 'en attente'),
('F03', 'U01', 'Refusée'),
('F03', 'U03', 'Acceptée'),
('F04', 'U01', 'Acceptée'),
('F05', 'U02', 'En attente'),
('F05', 'U03', 'En attente');

-- --------------------------------------------------------

--
-- Table structure for table `departement`
--

DROP TABLE IF EXISTS `departement`;
CREATE TABLE IF NOT EXISTS `departement` (
  `CodeDepartement` int(11) NOT NULL,
  `NomDepartement` varchar(50) NOT NULL,
  PRIMARY KEY (`CodeDepartement`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departement`
--

INSERT INTO `departement` (`CodeDepartement`, `NomDepartement`) VALUES
(2, 'Ile de France'),
(3, 'Nouvelle Aquitaine'),
(6, 'Antibes');

-- --------------------------------------------------------

--
-- Table structure for table `fonction`
--

DROP TABLE IF EXISTS `fonction`;
CREATE TABLE IF NOT EXISTS `fonction` (
  `idFonct` varchar(5) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`idFonct`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fonction`
--

INSERT INTO `fonction` (`idFonct`, `libelle`) VALUES
('F01', 'Salarié'),
('F02', 'Secrétaire'),
('F03', 'Responsable f');

-- --------------------------------------------------------

--
-- Table structure for table `formation`
--

DROP TABLE IF EXISTS `formation`;
CREATE TABLE IF NOT EXISTS `formation` (
  `idForma` varchar(5) NOT NULL,
  `intitule` varchar(30) NOT NULL,
  `descriptif` varchar(200) NOT NULL,
  `duree` int(11) NOT NULL,
  `dateOuvertInscriptions` date NOT NULL,
  `dateClotureInscriptions` date NOT NULL,
  `effectifMax` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`idForma`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formation`
--

INSERT INTO `formation` (`idForma`, `intitule`, `descriptif`, `duree`, `dateOuvertInscriptions`, `dateClotureInscriptions`, `effectifMax`) VALUES
('F01', 'Gestion Informatique', 'Formation permettant de tenir à jour la partie de la gestion informatique des ligues', 4, '2020-09-09', '2020-09-30', '6'),
('F02', 'Gestion des Clubs', 'S\'occuper du bien être et du bon fonctionnement d\'un club au niveau managérial ', 10, '2020-10-01', '2020-11-20', '4'),
('F03', 'Comptabilité', 'S\'occuper de la comptabilité des ligues (fiches de paye)', 10, '2020-07-14', '2020-12-19', '2'),
('F04', 'Intervenant évènement', 'Intervenant lors des jours de compétition , de journée porte ouverte à l\'écoute des clients ', 5, '2020-11-13', '2020-12-10', '20'),
('F05', 'Coaching', 'Coach de club , aimer le contact avec autrui et transmettre son savoir', 10, '2020-12-11', '2021-02-19', '10');

-- --------------------------------------------------------

--
-- Table structure for table `ligue`
--

DROP TABLE IF EXISTS `ligue`;
CREATE TABLE IF NOT EXISTS `ligue` (
  `idLigue` varchar(5) NOT NULL,
  `nomLigue` varchar(30) NOT NULL,
  `site` varchar(150) NOT NULL,
  `descriptif` varchar(65000) NOT NULL,
  PRIMARY KEY (`idLigue`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ligue`
--

INSERT INTO `ligue` (`idLigue`, `nomLigue`, `site`, `descriptif`) VALUES
('L1', 'Ligue Nouvelle Aquitaine', 'http://sites.ffkarate.fr/nouvelleaquitaine/la-ligue/', 'Bienvenue sur le nouveau site de la Ligue Régionale Nouvelle Aquitaine de Karaté et Disciplines Associées\r\nNous espérons qu’au fil des pages vous trouverez les renseignements que vous souhaitez?!\r\nDans tous les cas, nous restons à votre disposition.\r\nCe site est le votre, faites nous part de vos remarques.\r\n\r\nNous espérons pouvoir répondre à vos attentes et vous fournir les renseignements les plus utiles.'),
('L2', 'Ligue PACA', 'https://liguesudpaca.ffr.fr/ligue/presentation-de-la-ligue/la-ligue-et-ses-missions', 'La Ligue est née de la fusion entre les Comités Territoriaux de Provence et de Côte d’Azur, en application de la loi de territorialisation, dite loi NOTRe, devenant ainsi la seule institution du rugby sur le contour régional.\r\n\r\nLa Ligue Provence Alpes Côte d’Azur Sud de Rugby et le Conseil régional ont créé des liens étroits qui se renforcent jour après jour, le Conseil Régional devenant le premier partenaire de notre organisation. \r\nPour renforcer cette union, la ligue a adopté le même nom que la Région et devient Ligue Provence Alpes Côte d’Azur Sud de Rugby.\r\n\r\nNotre entité est le relais de la FFR sur notre territoire. Elle est chargée de la promotion, l’animation, l’initiation et la gestion du rugby sur 6 départements (Alpes Maritimes, Alpes de Haute Provence, Bouches du Rhône, Hautes Alpes, Var et Vaucluse) regroupant 120 clubs, dont deux professionnels (RCT et Provence Rugby Aix-Marseille), et 28 000 licenciés.'),
('L3', 'Ligue Paris ile de France', 'https://paris-idf.fff.fr/', 'Ligue ouverte à tous');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUser` varchar(5) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `login` varchar(20) NOT NULL,
  `mdp` varchar(128) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `typeUser` varchar(10) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `idFonct` varchar(5) NOT NULL,
  `idLigue` varchar(5) DEFAULT NULL,
  `idClub` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  KEY `utilisateur_fonction_FK` (`idFonct`),
  KEY `utilisateur_ligue0_FK` (`idLigue`),
  KEY `utilisateur_club1_FK` (`idClub`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`idUser`, `nom`, `prenom`, `login`, `mdp`, `typeUser`, `statut`, `idFonct`, `idLigue`, `idClub`) VALUES
('U01', 'Tiberti', 'Hugo', 'groot', 'ee79bb3a790bb6c76f854e897eff29b5', 'Salarié', 0, 'F01', 'L1', 'C2'),
('U02', 'Schiazza', 'Juju', 'Schiazza', '94785d9430119cac43880147fd91ab98', 'salarié', 0, 'F02', 'L2', 'C4'),
('U03', 'Rousselon', 'Zacharie', 'root', '63a9f0ea7bb98050796b649e85481845', 'salarié', 0, 'F03', 'L3', 'C5');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bulletin`
--
ALTER TABLE `bulletin`
  ADD CONSTRAINT `bulletin_contrat_FK` FOREIGN KEY (`idContrat`) REFERENCES `contrat` (`idContrat`);

--
-- Constraints for table `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `club_Commune0_FK` FOREIGN KEY (`IdCommune`) REFERENCES `commune` (`IdCommune`),
  ADD CONSTRAINT `club_ligue_FK` FOREIGN KEY (`idLigue`) REFERENCES `ligue` (`idLigue`);

--
-- Constraints for table `commune`
--
ALTER TABLE `commune`
  ADD CONSTRAINT `Commune_Departement_FK` FOREIGN KEY (`CodeDepartement`) REFERENCES `departement` (`CodeDepartement`);

--
-- Constraints for table `contrat`
--
ALTER TABLE `contrat`
  ADD CONSTRAINT `contrat_utilisateur_FK` FOREIGN KEY (`idUser`) REFERENCES `utilisateur` (`idUser`);

--
-- Constraints for table `demander`
--
ALTER TABLE `demander`
  ADD CONSTRAINT `Demander_formation_FK` FOREIGN KEY (`idForma`) REFERENCES `formation` (`idForma`),
  ADD CONSTRAINT `Demander_utilisateur0_FK` FOREIGN KEY (`idUser`) REFERENCES `utilisateur` (`idUser`);

--
-- Constraints for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_club1_FK` FOREIGN KEY (`idClub`) REFERENCES `club` (`idClub`),
  ADD CONSTRAINT `utilisateur_fonction_FK` FOREIGN KEY (`idFonct`) REFERENCES `fonction` (`idFonct`),
  ADD CONSTRAINT `utilisateur_ligue0_FK` FOREIGN KEY (`idLigue`) REFERENCES `ligue` (`idLigue`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
