-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 04 Septembre 2020 à 14:29
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `m2l`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUser` varchar(5) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `login` varchar(20) NOT NULL,
  `mdp` varchar(20) NOT NULL,
  `typeUser` varchar(10) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `idFonct` varchar(5) NOT NULL,
  `idLigue` varchar(5) DEFAULT NULL,
  `idClub` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  KEY `utilisateur_fonction_FK` (`idFonct`),
  KEY `utilisateur_ligue0_FK` (`idLigue`),
  KEY `utilisateur_club1_FK` (`idClub`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUser`, `nom`, `prenom`, `login`, `mdp`, `typeUser`, `statut`, `idFonct`, `idLigue`, `idClub`) VALUES
('u1', 'Hugp', 'Tiberti', 'Hugoleplubo', 'hugo<3', '', 0, '01', 'J1', 'j1');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `utilisateur_fonction_FK` FOREIGN KEY (`idFonct`) REFERENCES `fonction` (`idFonct`),
  ADD CONSTRAINT `utilisateur_ligue0_FK` FOREIGN KEY (`idLigue`) REFERENCES `ligue` (`idLigue`),
  ADD CONSTRAINT `utilisateur_club1_FK` FOREIGN KEY (`idClub`) REFERENCES `club` (`idClub`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
