-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 04 mai 2020 à 12:30
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `amap`
--
CREATE DATABASE IF NOT EXISTS `amap` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `amap`;

-- --------------------------------------------------------

--
-- Structure de la table `adherents`
--

DROP TABLE IF EXISTS `adherents`;
CREATE TABLE IF NOT EXISTS `adherents` (
  `idAdherent` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  PRIMARY KEY (`idAdherent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `adherents`
--

INSERT INTO `adherents` (`idAdherent`, `nom`, `prenom`) VALUES
(19, 'Leval', 'Céline'),
(26, 'Dubort', 'Joel'),
(27, 'Leblanc', 'Jean'),
(28, 'Humert', 'Elise');

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `idCategorie` int(11) NOT NULL AUTO_INCREMENT,
  `nomCategorie` varchar(40) NOT NULL,
  PRIMARY KEY (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`idCategorie`, `nomCategorie`) VALUES
(1, 'légumes'),
(2, 'fruits'),
(3, 'viandes'),
(4, 'boulangerie'),
(5, 'boissons'),
(6, 'crèmerie'),
(7, 'épicerie');

-- --------------------------------------------------------

--
-- Structure de la table `commandes`
--

DROP TABLE IF EXISTS `commandes`;
CREATE TABLE IF NOT EXISTS `commandes` (
  `idCommande` int(11) NOT NULL AUTO_INCREMENT,
  `dateCommande` date NOT NULL,
  `idVente` int(11) NOT NULL,
  `idAdherent` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCommande`),
  KEY `idVente` (`idVente`),
  KEY `idAdherent` (`idAdherent`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commandes`
--

INSERT INTO `commandes` (`idCommande`, `dateCommande`, `idVente`, `idAdherent`) VALUES
(1, '2019-03-19', 1, 19),
(2, '2019-03-19', 1, 26),
(3, '2019-03-26', 2, 19),
(4, '2019-03-02', 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `lignescommande`
--

DROP TABLE IF EXISTS `lignescommande`;
CREATE TABLE IF NOT EXISTS `lignescommande` (
  `idCommande` int(11) NOT NULL,
  `idProduit` int(11) NOT NULL,
  `quantite` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idCommande`,`idProduit`),
  KEY `idProduit` (`idProduit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `lignescommande`
--

INSERT INTO `lignescommande` (`idCommande`, `idProduit`, `quantite`) VALUES
(1, 2, '2.00'),
(1, 4, '4.00'),
(2, 1, '8.00'),
(2, 3, '1.00'),
(2, 4, '7.00'),
(2, 5, '4.00'),
(3, 3, '2.00');

-- --------------------------------------------------------

--
-- Structure de la table `producteurs`
--

DROP TABLE IF EXISTS `producteurs`;
CREATE TABLE IF NOT EXISTS `producteurs` (
  `idProducteur` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `adresseProducteur` varchar(50) NOT NULL,
  `communeProducteur` varchar(40) NOT NULL,
  `codePostalProducteur` varchar(5) NOT NULL,
  `descriptifProducteur` longtext NOT NULL,
  PRIMARY KEY (`idProducteur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `producteurs`
--

INSERT INTO `producteurs` (`idProducteur`, `nom`, `adresseProducteur`, `communeProducteur`, `codePostalProducteur`, `descriptifProducteur`) VALUES
(1, 'Pierre Malin', '9 chemin du moulin ', 'Lamothe-Montravel', '24230', 'Producteur de fruits et légumes en agriculture raisonnée.\r\nNous pratiquons la lutte biologique intégrale ( introduction de prédateurs contre les nuisibles), ce qui évite les insecticides.\r\nNous pratiquons un désherbage mécanique ou manuel, donc pas de désherbant anti-germinatif dans les cultures.\r\nTous nos produits sont choisit pour leur qualité gustative.'),
(2, 'Lucie Prulier', 'Route des pommiers', 'Lizac', '82200', 'Exploitation en agriculture raisonnée pour les fraises, cerises, abricots, pêches, pêches plates, brugnons, figues, prunes américaines et dérivé en jus de fruits, compotes, veloutés et vinaigre de pommes BIO\r\nexploitation en label BIO pour certaines variétés de pommes et pour des prunes.mise en conversion en 2017 pour les poires\r\nnous vous proposons en plus des légumes BIO comme les pommes de terre, les tomates,des courgettes rondes, les artichauts, les petits pois, les cornichons, concombres, aubergines, courges pour l\'hiver... '),
(3, 'Thierry Lefort', 'lieu dit les oiseaux', 'Duras', '47200', 'frais, bio, bon');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

DROP TABLE IF EXISTS `produits`;
CREATE TABLE IF NOT EXISTS `produits` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `nomProduit` varchar(50) NOT NULL,
  `descriptifProduit` longtext NOT NULL,
  `idProducteur` int(11) NOT NULL,
  `idCategorie` int(11) NOT NULL,
  PRIMARY KEY (`idProduit`),
  KEY `idProducteur` (`idProducteur`),
  KEY `idCategorie` (`idCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`idProduit`, `nomProduit`, `descriptifProduit`, `idProducteur`, `idCategorie`) VALUES
(1, 'Carottes', 'carottes lavées, produites en agriculture raisonnées', 1, 1),
(2, 'Céleri branche', 'Un pied de céleri branche (entre 800g et 1kg)', 1, 1),
(3, 'Courges Buttercup', 'Très bonne qualité gustative (proche du potimarron). Bonne conservation ', 1, 1),
(4, 'Pommes chantecler', 'Variété reinette jaune\r\nElle est croquante avec un arrière goût sucré/amer ', 2, 2),
(5, 'Longues de Nice', 'Une sorte de courgette longue collée à  une courgette ronde ! Sa taille peut varier de 60 cm à  1 m La chair ferme a un goût musqué et sucré. L\'épiderme, d\'abord vert, vire à l\'ocre à  maturité. Ils sont utilisés pour faire des soupes,des tartes de la purée et des gâteaux.', 2, 1),
(6, 'Pommes pitchounettes', 'Croquante, parfumÃ©e, juteuse, sucrÃ©e, douce...\r\nvariÃ©tÃ© de la famille des reinettes... calibre moyen', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `ventes`
--

DROP TABLE IF EXISTS `ventes`;
CREATE TABLE IF NOT EXISTS `ventes` (
  `idVente` int(11) NOT NULL AUTO_INCREMENT,
  `dateVente` date NOT NULL,
  PRIMARY KEY (`idVente`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ventes`
--

INSERT INTO `ventes` (`idVente`, `dateVente`) VALUES
(1, '2019-03-21'),
(2, '2019-03-28'),
(3, '2019-04-04');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commandes`
--
ALTER TABLE `commandes`
  ADD CONSTRAINT `commandes_ibfk_2` FOREIGN KEY (`idAdherent`) REFERENCES `adherents` (`idAdherent`),
  ADD CONSTRAINT `commandes_ibfk_3` FOREIGN KEY (`idVente`) REFERENCES `ventes` (`idVente`);

--
-- Contraintes pour la table `lignescommande`
--
ALTER TABLE `lignescommande`
  ADD CONSTRAINT `lignescommande_ibfk_1` FOREIGN KEY (`idCommande`) REFERENCES `commandes` (`idCommande`),
  ADD CONSTRAINT `lignescommande_ibfk_2` FOREIGN KEY (`idProduit`) REFERENCES `produits` (`idProduit`);

--
-- Contraintes pour la table `produits`
--
ALTER TABLE `produits`
  ADD CONSTRAINT `produits_ibfk_2` FOREIGN KEY (`idCategorie`) REFERENCES `categories` (`idCategorie`),
  ADD CONSTRAINT `produits_ibfk_3` FOREIGN KEY (`idProducteur`) REFERENCES `producteurs` (`idProducteur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
