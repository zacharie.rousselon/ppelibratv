<?php

class MatchDAO{
    
    public function lire(Match $match){
        $sql = "select * from match where idMatch = " . $match->getIdMatch();
        $match = DBConnex::getInstance()->queryFetchFirstRow($sql);
        return $match;
    }
    
    public function ajouter(Match $match){
        
    }
    
    public function supprimer(Match $match){
        
    }
    
    public function modifier(Match $match){
        
    }
    
    public static function creerMatchs($nbEquipes){
        $nbJournees = ($nbEquipes - 1)*2;
        for ( $i =1 ; $i <= $nbJournees ; $i++){
            for ($j = 1 ; $j <= $nbEquipes/2; $j++){
                $sql = "insert into rencontre (journee) values ( " . $i . ") ; ";
                DBConnex::getInstance()->insert($sql);
                echo $sql;
            }
        }
    }
    
    
    
    
    public static function lesMatchs(){
        $result = array();
        $sql = "SELECT * FROM `rencontre` ";
        $liste = DBConnex::getInstance()->queryFetchAll($sql);
        
        if(!empty($liste)){
            foreach($liste as $match){
                $unMatch = new Match();
                $unMatch->hydrate($match);
                $result[] = $unMatch;
            }
        }
        return $result;
    }
    
    public static function lesMatchsUneJournee($uneJournee){
        $result = array();
        $sql = "SELECT * FROM `rencontre` where journee = $uneJournee";
        $res = DBConnex::getInstance()->query($sql);
        $liste = $res->fetchAll(PDO::FETCH_ASSOC);
        
        if(!empty($liste)){
            foreach($liste as $match){
                $unMatch = new Match();
                $unMatch->hydrate($match);
                $result[] = $unMatch;
            }
        }
        return $result;
    }
}

