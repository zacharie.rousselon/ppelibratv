<?php
class UtilisateurDAO{
        
    public static function verification(Utilisateur $utilisateur){
        
        $requetePrepa = DBConnex::getInstance()->prepare("select login from utilisateur where login = :login and  mdp = md5(:mdp)");
        $requetePrepa->bindParam( ":login", $utilisateur->getLogin());
        $requetePrepa->bindParam( ":mdp" ,  $utilisateur->getMdp());
        
       $requetePrepa->execute();

       $login = $requetePrepa->fetch();
       return $login[0];
    }
    
}