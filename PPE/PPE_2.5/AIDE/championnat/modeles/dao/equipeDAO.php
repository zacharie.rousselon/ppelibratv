<?php
class EquipeDAO{
    
    
    public static function lire(Equipe $equipe){
  
        $requetePrepa = DBConnex::getInstance()->prepare("select * from Equipe where idEquipe = :idEquipe");
        $requetePrepa->bindParam( ":idEquipe", $equipe->getIdEquipe());       
        $requetePrepa->execute();
        return  $requetePrepa->fetch();   
    }
    
    
    public static function supprimer(Equipe $equipe){
        $requetePrepa = DBConnex::getInstance()->prepare("delete from equipe where idEquipe = :idEquipe");
        $requetePrepa->bindParam( ":idEquipe", $equipe->getIdEquipe());
        return $requetePrepa->execute();   
    }
    
       
    
    public static function modifier(Equipe $equipe){
             
    
        
    }
    
    public static function ajouter(Equipe $equipe){

        
        
    }
    
    
    
    public static function lesEquipes(){
        $result = [];
        $requetePrepa = DBConnex::getInstance()->prepare("select * from equipe order by nomEquipe " );
       
        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC); 
        
        if(!empty($liste)){
            foreach($liste as $equipe){
                $uneEquipe = new Equipe();
                $uneEquipe->hydrate($equipe);
                $result[] = $uneEquipe;
            }
        }
        return $result;
    }
    
    
   
    
    public static function lesMatchs(Equipe $equipe){
        
       
    }
    
    
    public static function lesEquipesRencontres(){
         
    

    }
    
    
   
    
}
