<?php
class Equipe{
    use Hydrate;
	private $idEquipe;
	private $nomEquipe;
	private $lesMatchs = [];

	public function __construct($unIdEquipe = NULL , $unNomEquipe = NULL){
		$this->idEquipe = $unIdEquipe;
		$this->nomEquipe = $unNomEquipe;
	}

	public function getIdEquipe(){
		return $this->idEquipe;
	}
	
	public function setIdEquipe($unIdEquipe){
	    $this->idEquipe =  $unIdEquipe;
	}

	public function getNomEquipe(){
		return $this->nomEquipe;
	}
	
	public function setNomEquipe($unNomEquipe){
		$this->nomEquipe = $unNomEquipe;
	}

	
}