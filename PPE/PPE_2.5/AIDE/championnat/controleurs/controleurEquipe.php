<?php

/*****************************************************************************************************
 * Instancier un objet contenant la liste des �quipes et le conserver dans une variable de session
 *****************************************************************************************************/
$_SESSION['listeEquipes'] = new Equipes(EquipeDAO::lesEquipes());
	
/*****************************************************************************************************
 * Conserver dans une variable de session l'item actif du menu equipe
 *****************************************************************************************************/
if(isset($_GET['equipe'])){
	$_SESSION['equipe']= $_GET['equipe'];
}
else
{
	if(!isset($_SESSION['equipe'])){
		$_SESSION['equipe']="0";
	}
}



/*****************************************************************************************************
 * Créer un menu vertical à partir de la liste des équipes
 *****************************************************************************************************/
$menuEquipe = new menu("menuEquipe");


foreach ($_SESSION['listeEquipes']->getEquipes() as $uneEquipe){
	$menuEquipe->ajouterComposant($menuEquipe->creerItemLien($uneEquipe->getNomEquipe() ,$uneEquipe->getIdEquipe(),"images/".$uneEquipe->getNomEquipe().".png"));
}

$leMenuEquipes = $menuEquipe->creerMenuEquipe($_SESSION['equipe']);


/*****************************************************************************************************
 * Récupérer l'équipe sélectionnée
 *****************************************************************************************************/
$equipeActive = $_SESSION['listeEquipes']->chercheEquipe($_SESSION['equipe']);

/*****************************************************************************************************
 * Créer un formulaire contenant les informations sur l'�quipe slectionn�e
 *****************************************************************************************************/
if ($_SESSION['equipe'] != "0")
{
    $formulaireEquipe = new Formulaire('post', 'index.php', 'formuEquipe', 'formuEquipe');
    $unComposant = $formulaireEquipe->creerLabelfor('nomEquipe', 'Nom :');
    $formulaireEquipe->ajouterComposantLigne($unComposant, 1);
    
    $unComposant = $formulaireEquipe->creerInputTexte('nomEquipe', 'nomEquipe', $equipeActive->getNomEquipe() ,1, "", 1);
    $formulaireEquipe->ajouterComposantLigne($unComposant,1);
    $formulaireEquipe->ajouterComposantTab();
}


include_once 'vues/squeletteEquipe.php';